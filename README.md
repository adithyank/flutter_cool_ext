<!--
This README describes the package. If you publish this package to pub.dev,
this README's contents appear on the landing page for your package.

For information about how to write a good package README, see the guide for
[writing package pages](https://dart.dev/guides/libraries/writing-package-pages).

For general information about developing packages, see the Dart guide for
[creating packages](https://dart.dev/guides/libraries/create-library-packages)
and the Flutter guide for
[developing packages and plugins](https://flutter.dev/developing-packages).
-->

This package has Lots of `Small Utility classes`, `methods` and `properties`
for your day to day Flutter coding.

This is a opinionated package with lots of utilities behaving in their own
style. Many methods and utilities offer customization. But, few may not.

Please raise MR/PR to me or create an `enhancement request` at
[this link](https://gitlab.com/adithyank/flutter_cool_ext/-/issues).
I will modify and push release next version with your requests.


## Features

### `BuildContext` Additional methods

1. `MediaQuery.of(context)` can be shortened as `context.mediaQuery`.
2. `Theme.of(context)` can be shortened as `context.theme`
3. `Navigator.of(context)` can be shortened as `context.navigator` 
4. `ScaffoldMessenger.of(context)` can be shortened as `context.scaffoldMessenger`



## `Date` utilities

## General Utilities

## `String` Additional Methods

## Lot of `UI Utilies`

## `Widget` Additional Methods

## `WidgetRefresher` widget

## `WidgetReplacer` widget

## all object Extension

```dart
    String 
    var obj =
```