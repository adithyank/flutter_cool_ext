## 2.21.0

1. MsgDisplay.textAlign property introduced

## 2.19.0

1. New date format added in DateUtil
2. LoadingButton: async error handled for clearing the circle
3. buildAndJoinWidgets: not empty check added
4. StringExt.takeAfterLast() method added to extract extension from file name strings
5. widgetOnColor customization added
6. OneTimeWorker: job marked done before calling to prevent the job execution more than once
7. NumExt added
8. UIUtil.networkErrorMsg Corected()

It was done to avoid showing the app review feeddback box frequently

## 2.18.0

ScaffoldMsngrStateExt extension added

## 2.17.0

1. SnackBarUtil friendly method added

## 2.16.0

1. SnackBarUtil.showWithMessenger() method added

## 2.15.0

1. SnackUtil class added

## 2.14.4

1. OrderPaymentStatus enum added
2. searchbox made searcable by typing
3. FolderView made selectable
4. new IConLabel widget added
5. ResponsiveWidget: customization improved
6. many dep version upgraded
7. ChoiceChips + others customizations improved

## 2.14.3

1. StringIdText.findAllById() method added

## 2.14.2

1. StringIdText.findById() method added to search by id
   in List<StringIdText>

## 2.14.1

1. MsgDisplay widget added

## 2.14.0

1. just dart coding guidelines
2. util methods
3. CoolList new widget added with lot of commonly required features
4. boldText(): softwrap done
5. DateUtil.addDays() method added
6. flutter 2.19 made min version
7. pp: breadcrumb like feature given
8. CoolUtil.wrapInFormField() customization enhanced
9. Choice Chips Customization added
10. copyable text icon size reduced
11. Date formatting: 0 value handled
12. clipboardCopyableText() made button compact
13. WidgetHolder widget added for Generic Replaceable container
14. navext: popAll issue resolved
15. LoadingButton buttonStyle customisation given

## 2.13.0

1. CoolUtil.clipboardCopyableText() icon size reduced to
   match font size
2. IteratbleExt.group() method added
3. Flutter sdk method used instead of custom impl for popping
   all and replacing one widget
4. NextBackStackController.setAsBase()
   method added to create Fresh route without pop all
5. Loading button tooltip support added
6. StringIdText.listFrom() null permitted for coding friendliness

## 2.12.0
1. NavigatorExt.removeUntilFirstAndPushWidget() issue resolved and method renamed

## 2.11.0
1. DateUtil added using `intl` dependency

## 2.10.0
1. NavigatorExt's one method renamed

## 2.9.0
1. NavigatorExt methods renamed

## 2.8.0
1. NavigatorExt written and few methods from BuildContext 
   moved to it

## 2.7.0
1. NextBack widget added
2. Animation Duration reduced in LoadingButton
3. String.mapIfEmpty(), CoolUtil.wrapInYesNoConfirmDialog() methods added

## 2.6.0
1. CoolUtil.disabledText*() methods added
2. ChipFilterableListView: addDivider support given
3. border eliminated for CoolUtil.wrapInFormField() method
4. CoolUtil.onInkCard() and listView() method options enhanced
5. BuildContextExt padding* and tooltip methods enhanced
6. null check done in tooltip ext
7. padding given during network error msg
8. type_conversion_util added
9. iconext.dart added
10. WidgetExt: tooltip and padding methods added
11. multiselectchips widget added
12. TypeIdText datastructure added
13. example project added
14. clipboard button widget split & listview title support added
15. ColorUtil class added
16. BuildContextExt.navigatorRemoveUntilFirstAndPush() method added
17. Few changes done to follow dart coding guidelines
18. CoolUtil.bodyLargeSize() method added
19. Default Nav animation changed from rtl to flutter default
20. various size methods added coolutil.dart
21. TypeConversionutil.dynamicListToStringSet() added
22. Friendly box menu widget added
23. IterableExt.mapToNullableList() method added


## 2.5.0
1. new MediaQueryExt class offers lot of friendly extension methods
   on MediaQueryData class
2. ChipFilterableList & ChoiceChips new widgets added

## 2.4.0
1. Flutter links improved as per pub.dev recommendation

## 2.3.0
1. DevConsole, IterableExt, DynamicExt utils added
2. Lints enabled


## 2.2.0
1. `intl` package dependency removed

## 2.1.0

1. License file updated

## 2.0.0

1. `fluttertoast` package dependency removed. It was used
    only in `CoolUtil.clipboardCopyableText()` method to show
    toast after copying test. This method was modified to give
    a call back after the copy operation.
2. It was felt that it is not worth having this package
    dependency for this small purpose. Also, the default toast
    style of this `fluttertoast` may not match with the overall
    application. Hence removal was given importance and removed


## 1.0.0

### Initial release with below utilities
1. `BuildContext` Additional methods
2. `Date` utilities
3. General Utilities
4. `String` Additional Methods
5. One liners to show `Toast`
6. Lot of `UI Utilies`
7. `Widget` Additional Methods
8. `WidgetRefresher` widget
9. `WidgetReplacer` widget

