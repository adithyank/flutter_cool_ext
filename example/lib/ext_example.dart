import 'package:cool_ext/cool_ext.dart';
import 'package:flutter/material.dart';

class ExampleWidget extends StatelessWidget {
  const ExampleWidget({super.key});

  @override
  Widget build(BuildContext context) {

    return Column(
      children: [
        Text(
          'Sample Text',
          style: context.theme.textTheme.titleLarge
        ),
        TextButton(
            onPressed: () => context.navigator.pushWidget(const NextPageWidget()),
            child: const Text('Navigate: Push')
        ),
        TextButton(
            onPressed: () => context.navigator.pushReplacementWidget(const NextPageWidget()),
            child: const Text('Navigate: Push Replace')
        ),
        TextButton(
            onPressed: () => context.scaffoldMessenger.showSnackBar(
                SnackBar(
                    content: Text('${context.mediaQuery.size}')
                )
            ),
            child: const Text('Screen Size in SnackBar')
        ),
        TextButton(
            onPressed: () => context.navigator.popUntilFirst(),
            child: const Text('Pop Until First Route')
        )
      ]
    );
  }
}

class NextPageWidget extends StatelessWidget {
  const NextPageWidget({super.key});
  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      body: Center(
        child: Text('Next Page')
      ),
    );
  }
}
