import 'package:cool_ext/cool_ext.dart';
import 'package:flutter/material.dart';

enum EditSaveMode
{
  defaultMode,
  editMode
}

class EditSaveButton extends StatefulWidget {
  final Future<bool> Function() onEditClicked;
  final Future<bool> Function() onSaveClicked;
  const EditSaveButton({
    required this.onEditClicked,
    required this.onSaveClicked, 
    super.key});

  @override
  State<EditSaveButton> createState() => _EditSaveButtonState();
}

class _EditSaveButtonState extends State<EditSaveButton> {

  late EditSaveMode curMode;
  
  @override
  void initState() {
    curMode = EditSaveMode.defaultMode;
    super.initState();
  }
  
  void changeMode(EditSaveMode mode)
  {
    setState(() {
      curMode = mode;
    });
  }
  
  @override
  Widget build(BuildContext context) {
    
    return LoadingButton(
        buttonType: LoadingButtonType.textButton,
        style: ButtonStyle(
          visualDensity: VisualDensity.compact
        ),
        onPressed: () async {
          if (curMode == EditSaveMode.defaultMode) {
            bool res = await widget.onEditClicked();
            if (res) {
              changeMode(EditSaveMode.editMode);
            }
          }
          else if (curMode == EditSaveMode.editMode) {
            bool res = await widget.onSaveClicked();
            if (res) {
              changeMode(EditSaveMode.defaultMode);
            }
          }
        },
        child: (curMode == EditSaveMode.defaultMode)
          ? Text('Edit')
            : Text('Save')
    );
  }
}
