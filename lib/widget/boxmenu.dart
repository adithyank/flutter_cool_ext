import 'dart:math';

import 'package:cool_ext/cool_ext.dart';
import 'package:flutter/material.dart';

class MenuBoxItem
{
  Widget icon;
  String labelText;
  Color bgColor;
  VoidCallback onPressed;

  MenuBoxItem({required this.onPressed, required this.icon, required this.labelText, this.bgColor = Colors.white});
}

class MenuBoxes extends StatelessWidget {
  final List<MenuBoxItem> menus;
  final int minCrossAxisCount;

  const MenuBoxes({required this.menus, required this.minCrossAxisCount,
    super.key});

  Widget buildMenu(BuildContext context, MenuBoxItem menu)
  {
    return CoolUtil.onInkCard(
      onTap: menu.onPressed,
      cardColor: menu.bgColor,
      child: Column(
        //mainAxisAlignment: MainAxisAlignment.center,
        children: [
          const SizedBox(height: 10),
          menu.icon.paddingAll(10),
          Center(
            child: Text(
                menu.labelText,
                textAlign: TextAlign.center
            ).paddingAll(10),
          ).expanded()
        ]
      ),
    );
  }

  int crossAxisBoxCount(BuildContext context)
  {
    double count = context.mediaQuery.size.width / 170.0;
    return max(minCrossAxisCount, count.toInt());
  }

  @override
  Widget build(BuildContext context) {
    return GridView.count(
        crossAxisCount: crossAxisBoxCount(context),
      crossAxisSpacing: 10,
      mainAxisSpacing: 10,
      children: menus.map((e) => buildMenu(context, e)).toList(),
    ).paddingAll(20);
  }
}

