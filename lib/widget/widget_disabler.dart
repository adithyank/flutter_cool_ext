import 'package:flutter/material.dart';

class WidgetDisabler extends StatefulWidget {
  final bool disable;
  final Widget child;
  const WidgetDisabler({required this.disable, required this.child, super.key});

  @override
  State<WidgetDisabler> createState() => _WidgetDisablerState();
}

class _WidgetDisablerState extends State<WidgetDisabler> {

  @override
  Widget build(BuildContext context) {
    Widget w = AbsorbPointer(
      absorbing: widget.disable,
      child: widget.child,
    );

    if (!widget.disable) {
      return w;
    }

    return Stack(
      children: [
        w,
        Container(
          width: double.infinity,
          height: double.infinity,
          color: Colors.white.withOpacity(0.3),
        )
      ]
    );
  }
}
