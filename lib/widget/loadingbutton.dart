import 'package:cool_ext/cool_ext.dart';
import 'package:flutter/material.dart';

enum LoadingButtonType
{
  elevatedButton,
  textButton,
  iconButton,
  filledButton
}

class LoadingButtonController
{
  late _LoadingButtonState _state;

  void showLoader()
  {
    DevConsole.printIfDebug('show loader');
    _state.showLoader();
  }

  void hideLoader()
  {
    DevConsole.printIfDebug('hide loader');
    _state.hideLoader();
  }
}

class LoadingButton extends StatefulWidget {
  final Future<void> Function()? onPressed;
  final Widget child;
  final LoadingButtonType buttonType;
  final String? tooltip;
  final ButtonStyle? style;

  const LoadingButton({required this.buttonType, required this.onPressed,
    this.tooltip, required this.child, this.style, super.key});

  @override
  State<LoadingButton> createState() => _LoadingButtonState();
}

class _LoadingButtonState extends State<LoadingButton> {
  LoadingButtonController controller = LoadingButtonController();
  bool loading = false;

  @override
  void initState() {
    loading = false;
    controller._state = this;
    super.initState();
  }

  void showLoader()
  {
    setState(() {
      loading = true;
    });
  }

  void hideLoader()
  {
    setState(() {
      loading = false;
    });
  }

  void onPressed()
  {
    //DevConsole.printIfDebug('onpressed first line');
    //showLoader();
    //DevConsole.printIfDebug('calling onpressed');
    showLoader();
    widget.onPressed!().then(
            (value) => hideLoader(),
            onError: (a) => hideLoader()
    ).onError((error, stackTrace) => hideLoader());
    //DevConsole.printIfDebug('returned onpressed');
    //hideLoader();
    //DevConsole.printIfDebug('onpressed last line');
  }

  @override
  Widget build(BuildContext context) {

    double? size = context.theme.textTheme.bodyLarge!.fontSize;

    VoidCallback? action = widget.onPressed == null || loading ? null : onPressed;
    Widget c = loading ? SizedBox(height: size, width: size, child: const CircularProgressIndicator(strokeWidth: 2)) : widget.child;
    c = AnimatedSize(duration: Duration(milliseconds: 200), child: c);

    Widget b;

    if (widget.buttonType == LoadingButtonType.textButton) {
      b = TextButton(onPressed: action, style: widget.style, child: c).tooltipMsg(widget.tooltip);
    }
    else if (widget.buttonType == LoadingButtonType.elevatedButton) {
      b = ElevatedButton(onPressed: action, style: widget.style, child: c).tooltipMsg(widget.tooltip);
    }
    else if (widget.buttonType == LoadingButtonType.filledButton) {
      b = FilledButton(onPressed: action, style: widget.style, child: c).tooltipMsg(widget.tooltip);
    }
    else { //iconbutton
      b = IconButton(onPressed: action, style: widget.style, icon: c, tooltip: widget.tooltip);
    }

    return b;
  }
}

