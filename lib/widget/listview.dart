import 'package:cool_ext/cool_ext.dart';
import 'package:flutter/material.dart';

class ListReOrderHelper<T>
{
  String Function(T item) keyProvider;
  void Function(int oldIndex, int newIndex, List<T> reOrderedList) onReOrder;

  ListReOrderHelper({required this.keyProvider, required this.onReOrder});
}

class CoolList<T> extends StatefulWidget {
  final List<T> items;
  final Widget Function(T item) widgetBuilder;
  final String msgOnEmpty;
  final String? title;
  final bool addDivider;
  final bool startWithDivider;
  final double gap;
  final bool shrinkWrap;
  final Color dividerColor;
  final Widget? titleWidget;
  final ListReOrderHelper<T>? listReOrderHelper;

  CoolList({
    required this.items,
    required this.widgetBuilder,
    this.msgOnEmpty = 'Empty List',
    this.title,
    this.addDivider = true,
    this.startWithDivider = true,
    this.gap = 0,
    this.shrinkWrap = false,
    this.dividerColor = Colors.grey,
    this.titleWidget,
    this.listReOrderHelper,
    super.key,
  });

  @override
  State<CoolList<T>> createState() => _CoolListState<T>();
}

class _CoolListState<T> extends State<CoolList<T>> {

  List<T> curItems = [];

  @override
  void initState() {
    curItems.clear();
    curItems.addAll(widget.items);
    super.initState();
  }
  @override
  Widget build(BuildContext context) {

    if (curItems.isEmpty) {
      return Center(child: Text(widget.msgOnEmpty));
    }

    Widget interleaver = widget.addDivider
        ? Divider(thickness: 0, height: 1, color: widget.dividerColor)
        : SizedBox(height: widget.gap);

    List<Widget> children = [
      if (widget.titleWidget != null)
        widget.titleWidget!,

      ...curItems.mapToList((item) {
        Widget w = widget.widgetBuilder(item);

        return Column(
          key: widget.listReOrderHelper == null ? null : ValueKey(widget.listReOrderHelper!.keyProvider(item)),
          mainAxisSize: MainAxisSize.min,
          children: [
            w,
            interleaver
          ],
        );
      })
    ];

    Widget child;

    if (widget.listReOrderHelper != null) {
      child = ReorderableListView(
          children: children,
          onReorder: (oldIndex, newIndex) {
            //DevConsole.printIfDebug('oldIndex: $oldIndex, newIndex: $newIndex');
            setState(() {
              if (oldIndex < newIndex) {
                newIndex -= 1;
              }
              final T item = curItems.removeAt(oldIndex);
              curItems.insert(newIndex, item);
            });
            widget.listReOrderHelper!.onReOrder(oldIndex, newIndex, curItems);
          }
      );
    }
    else {
      child = ListView(
          padding: EdgeInsets.zero,
          shrinkWrap: widget.shrinkWrap,
          children: children
      );
    }

    if (widget.title.isNullOrEmpty) {
      return child;
    }

    return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Text(widget.title!, style: Theme.of(context).textTheme.titleLarge).paddingHorizontal(10),
          const SizedBox(height: 10),
          child.expanded()
        ]
    );
  }
}
