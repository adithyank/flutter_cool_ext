import 'dart:async';

import 'package:flutter/material.dart';

class WidgetRefresher extends StatefulWidget {

  final Duration duration;
  final WidgetBuilder builder;

  const WidgetRefresher({required this.duration, required this.builder, Key? key}) : super(key: key);

  @override
  State<WidgetRefresher> createState() => _WidgetRefresherState();
}

class _WidgetRefresherState extends State<WidgetRefresher> {

  late Timer timer;

  void refresh()
  {
    setState(() {
    });
  }

  @override
  void initState() {
    super.initState();
    timer = Timer.periodic(widget.duration, (timer) => refresh());
  }

  @override
  void dispose() {
    super.dispose();
    timer.cancel();
  }

  @override
  Widget build(BuildContext context) {
    return widget.builder(context);
  }
}
