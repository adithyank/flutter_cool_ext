import 'package:cool_ext/cool_ext.dart';
import 'package:cool_ext/widget/widget_holder.dart';
import 'package:flutter/material.dart';

class NextBackStack extends StatefulWidget {
  final Widget child;
  final NextBackStackController controller;
  final VoidCallback? onBack;
  final bool showHeader;

  NextBackStack({super.key, required this.controller, required this.child,
    this.onBack, this.showHeader = false});

  @override
  State createState() => NextBackStackState();
}

abstract class NextBackChildWidget
{
  Widget? header(BuildContext context);
}

class NextBackStackController {
  late NextBackStackState state;
  WidgetHolderController headerController = WidgetHolderController();

  void show(Widget widget, {Widget? header, VoidCallback? onBack}) {
    state.show(widget, header: header, onBack: onBack);
  }

  void setAsBase(Widget widget, {Widget? header, VoidCallback? onBack}) {
    state.showAsBase(widget, header: header, onBack: onBack);
  }

  Widget backButton()
  {
    return IconButton(
        onPressed: () => back(),
        icon: const Icon(Icons.arrow_back),
        iconSize: 25,
        color: Colors.black87,
      tooltip: 'Back',
    );
  }

  void back() {
    state.back();
  }

  Widget headerWidget()
  {
    return WidgetHolder(controller: headerController);
  }
}

class WidgetAndAction
{
  Widget w;
  VoidCallback? onBack;
  final Widget? _header;

  WidgetAndAction(this.w, this.onBack, this._header);

  Widget get headerWidget => _header == null ? Icon(Icons.circle, size: 10, color: Colors.grey) : _header!;
}

class NextBackStackState extends State<NextBackStack> {
  List<WidgetAndAction> widgets = [];
  late int curIndex;

  @override
  void initState() {
    super.initState();
    widget.controller.state = this;
    widgets.add(WidgetAndAction(widget.child, widget.onBack, null));
    widget.controller.headerController.child = headerRow();
    //updateHeader();
    curIndex = 0;
  }


  void show(Widget bottomWidget, {Widget? header, VoidCallback? onBack}) {
    Widget w = buildWidget(bottomWidget, header: header);
    setState(() {
      widgets.add(WidgetAndAction(w, onBack, header));
      curIndex++;
      updateHeader();
    });
  }

  void updateHeader()
  {
    widget.controller.headerController.showChild(headerRow());
  }

  Widget headerRow()
  {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: CoolUtil.buildAndJoinWidgets<WidgetAndAction>(
          items: widgets,
          widgetBuilder: (item) => item.headerWidget,
          interleaved: Icon(Icons.arrow_right_outlined, color: Colors.grey),
          startWithInterleaver: false,
          endWithInterleaver: false
      ),
    );
  }

  void showAsBase(Widget bottomWidget, {Widget? header, VoidCallback? onBack})
  {
    Widget w = buildWidget(bottomWidget, header: header);

    setState(() {
      widgets.clear();
      curIndex = 0;
      widgets.add(WidgetAndAction(w, onBack, header));
      updateHeader();
    });
  }

  Widget buildWidget(Widget bottomWidget, {Widget? header})
  {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        if (widget.showHeader)
          Row(
            children: [
              if (curIndex > 0)
                widget.controller.backButton(),
              const SizedBox(width: 10),
              widget.controller.headerWidget()
            ]
          ).paddingHorizontal(10),
        bottomWidget.expanded()
      ]
    );
  }

  void back() {
    if (curIndex == 0) return;

    setState(() {
      WidgetAndAction removed = widgets.removeLast();
      curIndex--;
      updateHeader();
      if (removed.onBack != null) {
        removed.onBack!();
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return IndexedStack(
      index: curIndex,
      children: widgets.map((e) => e.w).toList(),
    );
  }
}
