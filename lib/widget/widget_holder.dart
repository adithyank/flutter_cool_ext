import 'package:flutter/material.dart';

class WidgetHolderController extends ChangeNotifier
{
  Widget? child;

  void showChild(Widget child)
  {
    this.child = child;
    notifyListeners();
  }
}

class WidgetHolder extends StatefulWidget {
  final WidgetHolderController controller;

  const WidgetHolder({required this.controller, super.key});

  @override
  State<WidgetHolder> createState() => _WidgetHolderState();
}

class _WidgetHolderState extends State<WidgetHolder> {
  @override
  Widget build(BuildContext context) {
    return ListenableBuilder(
        listenable: widget.controller,
        builder: (context, child) => widget.controller.child ?? Center(child: Text('')),
    );
  }
}
