import 'package:flutter/material.dart';

class WidgetReplacerController<D>
{
  late _WidgetReplacerState<D> _state;

  void replace(D? newData)
  {
    _state.replace(newData);
  }

  void refreshWithOldData()
  {
    _state.refreshWithOldData();
  }

}

class WidgetReplacer<D> extends StatefulWidget {
  final WidgetReplacerController<D> controller;
  final D? data;
  final Widget Function(D? data) widgetBuilder;

  const WidgetReplacer({required this.controller, this.data, required this.widgetBuilder, Key? key}) : super(key: key);

  @override
  State<WidgetReplacer<D>> createState() => _WidgetReplacerState<D>();
}

class _WidgetReplacerState<D> extends State<WidgetReplacer<D>> {

  D? data;

  @override
  void initState() {
    widget.controller._state = this;
    data = widget.data;
    super.initState();
  }

  void replace(D? newData)
  {
    setState(() {
      data = newData;
    });
  }

  void refreshWithOldData()
  {
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return widget.widgetBuilder(data);
  }
}
