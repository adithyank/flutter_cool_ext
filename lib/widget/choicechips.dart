import 'package:cool_ext/cool_ext.dart';
import 'package:flutter/material.dart';

class ChoiceChipsController
{
  late _ChoiceChipsState _state;

  List<StringIdText> currentSelections()
  {
    return _state.curSelection;
  }
}

class ChoiceChips extends StatefulWidget {

  final List<StringIdText> items;
  final List<StringIdText>? initialSelection;
  final bool multiSelect;
  final void Function(List<StringIdText> currentSelection)? onSelectionChanged;
  final ChoiceChipsController? controller;
  final Color? selectedColor;
  final Color? selectedFgColor;
  final bool showCheckmark;
  final Color? fgColor;
  final Color? color;
  final BorderRadiusGeometry? borderRadius;

  const ChoiceChips({required this.items, this.initialSelection, this.multiSelect = false,
    this.controller, super.key, this.onSelectionChanged, this.selectedColor,
    this.selectedFgColor, this.showCheckmark = true, this.borderRadius,
    this.fgColor, this.color
  });

  @override
  State<ChoiceChips> createState() => _ChoiceChipsState();
}

class _ChoiceChipsState extends State<ChoiceChips> {

  List<StringIdText> curSelection = [];
  late BorderRadiusGeometry borderRadius;

  @override
  void initState() {
    borderRadius = widget.borderRadius ?? BorderRadius.circular(20);

    if (widget.controller != null) {
      widget.controller!._state = this;
    }

    if (widget.initialSelection.hasValue) {
      curSelection.addAll(widget.initialSelection!);
    }

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        Wrap(
            runSpacing: 0,
            children: CoolUtil.buildAndJoinWidgets<StringIdText>(
              items: widget.items,
              widgetBuilder: (item) => chip(context, item),
              interleaved: const SizedBox(width: 3),
              startWithInterleaver: false,
            )
        ).expanded(),
      ],
    );
  }

  void onClick(bool selected, StringIdText item)
  {
    setState(() {
      if (selected) {

        if (!widget.multiSelect) {
          curSelection.clear();
        }
        curSelection.add(item);
      }
      else {
        curSelection.remove(item);
      }
    });

    widget.onSelectionChanged?.call(curSelection);
  }

  Widget chip(BuildContext context, StringIdText item)
  {
    bool sel = curSelection.contains(item);

    return ChoiceChip(
      showCheckmark: widget.showCheckmark,
      visualDensity: VisualDensity.compact,
      //padding: EdgeInsets.zero,
      label: Text(item.text),
      labelStyle: sel
          ? (widget.selectedFgColor == null ? null : TextStyle(color: widget.selectedFgColor))
          : (widget.fgColor == null         ? null : TextStyle(color: widget.fgColor)),
      selected: sel,
      selectedColor: widget.selectedColor,
      backgroundColor: widget.color,
      onSelected: (selected) => onClick(selected, item),
      side: sel ? BorderSide.none : BorderSide(color: Colors.grey.shade300),
      shape: RoundedRectangleBorder(
          borderRadius: borderRadius
      ),
    );
  }
}

