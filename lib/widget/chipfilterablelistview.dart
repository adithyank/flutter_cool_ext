import 'package:cool_ext/cool_ext.dart';
import 'package:flutter/material.dart';

class ChipFilterableListItem
{
  Set<String> filterIds;
  Widget widget;

  ChipFilterableListItem(this.filterIds, this.widget);
}

//TODO: dynamic filters later. i.e., call back to return the items when the chip
//is clicked
class ChipFilterableListView extends StatefulWidget {

  static String allFilterId = '__all';
  
  final List<StringIdText> filters;
  final List<ChipFilterableListItem> items;
  final bool allFilter;
  final String allFilterText;
  final bool addDivider;

  const ChipFilterableListView({required this.filters, required this.items, this.allFilter = true, this.allFilterText = 'All', super.key, this.addDivider = false});

  @override
  State<ChipFilterableListView> createState() => _ChipFilterableListViewState();
}

class _ChipFilterableListViewState extends State<ChipFilterableListView> {
  StringIdText? allFilter;
  late List<ChipFilterableListItem> curList;
  List<StringIdText> totalFilters = [];
  Set<String> curActiveFilterIds = {};

@override
  void initState() {
    
    totalFilters.clear();
    if (widget.allFilter) {
      allFilter = StringIdText(ChipFilterableListView.allFilterId, widget.allFilterText);
      totalFilters.add(allFilter!);
    }

    totalFilters.addAll(widget.filters);

    curActiveFilterIds.clear();
    curActiveFilterIds.add(allFilter == null ? totalFilters[0].id : allFilter!.id);
    identifyCurList(false);
    super.initState();
  }
  
  void identifyCurList(bool doSetState)
  {
    bool all = curActiveFilterIds.length == 1 && curActiveFilterIds.contains(ChipFilterableListView.allFilterId);
    if (all) {
      curList = widget.items;
    }
    else {
      curList = widget.items.where((item) => item.filterIds.intersection(curActiveFilterIds).isNotEmpty).toList();
    }

    if (doSetState) {
      setState(() {});
    }
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        ChoiceChips(
            items: totalFilters,
            onSelectionChanged: (currentSelection) {
              setState(() {
                curActiveFilterIds = currentSelection.map((e) => e.id).toSet();
                identifyCurList(true);
              });
          }
        ).paddingHorizontal(10),
        Row(
          children: [
            CoolUtil.textOnColor(
              context,
              text: 'Showing ${curList.length} of ${widget.items.length}',
              backgroundColor: Colors.transparent
            )
          ]
        ),
        CoolUtil.listView<ChipFilterableListItem>(
            context,
            items: curList,
            widgetBuilder: (item) => item.widget,
          addDivider: widget.addDivider,
          startWithDivider: false
        ).expanded()
      ]
    );
  }
}

