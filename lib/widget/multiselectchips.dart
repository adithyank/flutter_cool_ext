import 'package:cool_ext/cool_ext.dart';
import 'package:flutter/material.dart';


class MultiSelectChipsController
{
  late ChoiceChipsController choiceChipsController;

  List<StringIdText> currentSelections()
  {
    return choiceChipsController.currentSelections();
  }
}

class MultiSelectChips extends StatelessWidget {
  final List<StringIdText> items;
  final List<StringIdText>? initialSelection;
  final MultiSelectChipsController? controller;

  final ChoiceChipsController choiceChipsController = ChoiceChipsController();

  MultiSelectChips({required this.items,
      this.initialSelection, super.key, this.controller}) {
    if (controller != null) {
      controller!.choiceChipsController = choiceChipsController;
    }
  }

  @override
  Widget build(BuildContext context) {
    return ChoiceChips(
        items: items,
      initialSelection: initialSelection,
      multiSelect: true,
      controller: choiceChipsController,
    );
  }
}

