import 'package:cool_ext/cool_ext.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class DateUtil
{
  static final DateFormat hh24mmss = DateFormat(DateFormat.HOUR24_MINUTE_SECOND);
  static final DateFormat hh12mma = DateFormat('hh:mm a');
  static final DateFormat ddmmyyyy = DateFormat('dd-MMM-y');
  static final DateFormat yyyymmm = DateFormat('y-MMM');
  static final DateFormat yyyymm = DateFormat('y-MM');
  static final DateFormat yyyymmdd = DateFormat('yMMdd');
  static final DateFormat ddmmmyyyy = DateFormat('dd-MMM-y');
  static final DateFormat ddmmm = DateFormat('dd-MMM');
  static final DateFormat ddmmyhhmmssaaa = DateFormat('dd-MMM-y HH:mm:ss');
  static final DateFormat ddmmyhhmma = DateFormat('dd-MMM-y h:mm a');
  static final DateFormat yyyymmddhhmmssForFileName = DateFormat('y-MM-dd-HH-mm-ss');


  static String formatDateHHMMSS(int millis) {
    return hh24mmss.format(DateTime.fromMillisecondsSinceEpoch(millis));
  }

  static String formatDateFriendlyDateAndTimeUptoMillis(int millis) {
    return '${ddmmyhhmmssaaa.format(DateTime.fromMillisecondsSinceEpoch(millis))}:${millis % 1000}';
  }

  static String formatDateDDMMMYHHMMSS(int millis) {
    if (millis <= 0) {
      return '-';
    } else {
      return ddmmyhhmmssaaa.format(DateTime.fromMillisecondsSinceEpoch(millis));
    }
  }

  static String getElapsedStr(Duration duration, {bool showMilliSeconds = false})
  {
    int d = duration.inMilliseconds ~/ Duration.millisecondsPerDay;

    int rem = duration.inMilliseconds - (d * Duration.millisecondsPerDay);

    int h = rem ~/ Duration.millisecondsPerHour;

    rem = rem - (h * Duration.millisecondsPerHour);

    int m = rem ~/ Duration.millisecondsPerMinute;

    rem = rem - (m * Duration.millisecondsPerMinute);

    int s = rem ~/ Duration.millisecondsPerSecond;

    rem = rem - (s * Duration.millisecondsPerSecond);

    int ms = rem;

    List<String> res = [];

    if (d > 0) {
      res.add('${d}d');
    }

    if (h > 0) {
      res.add('${h}hr');
    }

    if (m > 0) {
      res.add('${m}m');
    }

    if (s > 0) {
      res.add('${s}s');
    }

    if (showMilliSeconds) {
      if (ms > 0) {
        res.add('${ms}ms');
      }
    }
    
    return res.isNotEmpty ? res.join(' ') : '0s';
  }

  static TimeOfDay parseHHColonMMToTimeOfDay(String? hhColonMM)
  {
    if (hhColonMM == null || hhColonMM.isEmpty)
      return TimeOfDay(hour: 0, minute: 0);

    String hh = hhColonMM.substring(0, hhColonMM.indexOf(':'));
    String mm = hhColonMM.takeAfter(':');
    return TimeOfDay(hour: int.parse(hh), minute: int.parse(mm));
  }

  static DateTime addDaysAndSetHHMM(DateTime ref, int days, String hhColonMM)
  {
    var timeOfDay = parseHHColonMMToTimeOfDay(hhColonMM);
    return ref.add(Duration(days: days))
        .copyWith(hour: timeOfDay.hour, minute: timeOfDay.minute, second: 0, millisecond: 0);
  }

  static DateTime reduceDaysAndSetHHMM(DateTime ref, int days, String hhColonMM)
  {
    var timeOfDay = parseHHColonMMToTimeOfDay(hhColonMM);
    return ref.subtract(Duration(days: days))
        .copyWith(hour: timeOfDay.hour, minute: timeOfDay.minute, second: 0, millisecond: 0);
  }

}

extension IntExt on int
{
  String get asDDMMMYHHMMSSAAA  => this <= 0 ? '-' : DateUtil.formatDateFriendlyDateAndTimeUptoMillis(this);
  String get asDDMMMYHHMMSS     => this <= 0 ? '-' : DateUtil.formatDateDDMMMYHHMMSS(this);
  String get asHHMMSS           => this <= 0 ? '-' : DateUtil.hh24mmss.format(DateTime.fromMillisecondsSinceEpoch(this));
  String get asHHMMA           => this <= 0 ? '-' : DateUtil.hh12mma.format(DateTime.fromMillisecondsSinceEpoch(this));
  String get asDDMMYYYY         => this <= 0 ? '-' : DateUtil.ddmmyyyy.format(DateTime.fromMillisecondsSinceEpoch(this));
  String get asDDMMM         => this <= 0 ? '-' : DateUtil.ddmmm.format(DateTime.fromMillisecondsSinceEpoch(this));
  String get asDDMMYYYYHHMMA    => this <= 0 ? '-' : DateUtil.ddmmyhhmma.format(DateTime.fromMillisecondsSinceEpoch(this));
  String get asYYYYMMM          => this <= 0 ? '-' : DateUtil.yyyymmm.format(DateTime.fromMillisecondsSinceEpoch(this));
  String get asYYYYMM          => this <= 0 ? '-' : DateUtil.yyyymm.format(DateTime.fromMillisecondsSinceEpoch(this));
  String get yyyymmddhhmmssForFileName     => this <= 0 ? '-' : DateUtil.yyyymmddhhmmssForFileName.format(DateTime.fromMillisecondsSinceEpoch(this));
}


