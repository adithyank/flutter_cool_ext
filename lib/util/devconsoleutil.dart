import 'dart:developer' as dev;

import 'package:flutter/foundation.dart';

class DevConsole
{
  static const bool useDevLog = false;
  
  static void printCaught(String msg, Object ex, StackTrace? stackTrace)
  {
    if (useDevLog) {
      dev.log(msg, error: ex, stackTrace: stackTrace);
    }
    else {
      debugPrintStack(stackTrace: stackTrace, label: '$msg : ${ex.runtimeType} : $ex');
    }
  }

  static void printAlways(String s)
  {
    if (useDevLog) {
      dev.log(s);
    }
    else {
      debugPrint(s);
    }
  }

  static void printIfDebug(String s)
  {
    if (kDebugMode) {
      printAlways(s);
    }
  }


}