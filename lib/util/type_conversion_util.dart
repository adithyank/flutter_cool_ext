class TypeConversionUtil
{
  static List<String> dynamicListToStringList(List<dynamic>? dynamicList)
  {
    return dynamicList == null
        ? List.empty()
        : dynamicList.map((e) => e as String).toList();
  }

  static Set<String> dynamicListToStringSet(List<dynamic>? dynamicList)
  {
    return dynamicList == null
        ? Set()
        : dynamicList.map((e) => e as String).toSet();
  }

  static List<int> dynamicListToIntList(List<dynamic>? dynamicList)
  {
    return dynamicList == null
        ? List.empty()
        : dynamicList.map((e) => e as int).toList();
  }
}

