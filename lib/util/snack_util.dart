import 'package:cool_ext/cool_ext.dart';
import 'package:flutter/material.dart';

class SnackBarUtil {

  static Color successColor = Colors.green.shade900;
  static Color failureColor = Colors.orange.shade900;

  static void showSuccessWithMessenger(ScaffoldMessengerState scaffoldMessengerState, String message)
  {
    showWithMessenger(scaffoldMessengerState, message, true);
  }

  static void showFailureWithMessenger(ScaffoldMessengerState scaffoldMessengerState, String message)
  {
    showWithMessenger(scaffoldMessengerState, message, false);
  }

  static void showWithMessenger(ScaffoldMessengerState scaffoldMessengerState, String message, bool success)
  {
    Color bg = success ? successColor : failureColor;
    scaffoldMessengerState.showSnackBar(SnackBar(
        content: Text(message, style: const TextStyle(color: Colors.white)),
        backgroundColor: bg
    ));
  }

  static void show(BuildContext context, String message, bool success) {
    showWithMessenger(context.scaffoldMessenger, message, success);
  }

  static void showSuccess(BuildContext context, String message) {
    show(context, message, true);
  }

  static void showError(BuildContext context, String message) {
    show(context, message, false);
  }
}

extension ScaffoldMsngrStateExt on ScaffoldMessengerState
{
  void coolFailureSnackBar(String msg)
  {
    SnackBarUtil.showFailureWithMessenger(this, msg);
  }

  void coolSuccessSnackBar(String msg)
  {
    SnackBarUtil.showSuccessWithMessenger(this, msg);
  }
}