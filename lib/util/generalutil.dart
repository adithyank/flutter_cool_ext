import 'package:cool_ext/cool_ext.dart';

class StringIdText {
  String id;
  String text;

  StringIdText(this.id, this.text);

  StringIdText.empty() : id = '', text = '';

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'text': text,
    };
  }

  factory StringIdText.fromJson(Map<String, dynamic> map)
  {
    return StringIdText(map['id'], map['text']);
  }

  static StringIdText? nullableFromJson(Map<String, dynamic>? map)
  {
    return (map == null || map.isEmpty)
        ? null
        : StringIdText.fromJson(map);
  }

  static List<StringIdText> listFrom(List<dynamic>? list)
  {
    return list == null
      ? []
      : list.map((e) => StringIdText.fromJson(e)).toList();
  }

  static List<StringIdText>? nullableListFrom(List<dynamic>? list)
  {
    return list == null ? null : listFrom(list);
  }

  static StringIdText? findById(List<StringIdText> stringIdTexts, String? id)
  {
    if (id == null) {
      return null;
    }

    for (StringIdText sit in stringIdTexts)
    {
      if (sit.id == id) {
        return sit;
      }
    }
    return null;
  }

  static List<StringIdText> findAllById(List<StringIdText> stringIdTexts, List<String> ids)
  {
    Map<String, StringIdText> map = stringIdTexts.toMap((item) => item.id);

    return ids.map((e) => map[e])
        .where((element) => element != null)
        .map((e) => e!)
        .toList();
  }

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
          other is StringIdText &&
              runtimeType == other.runtimeType &&
              id == other.id &&
              text == other.text;

  @override
  int get hashCode => id.hashCode ^ text.hashCode;

  @override
  String toString() {
    return '$id:$text';
  }

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'text': text
    };
  }
}

class TypeIdText
{
  String type;
  String id;
  String text;

  TypeIdText(this.type, this.id, this.text);

  factory TypeIdText.fromMap(Map<String, dynamic> obj)
  {
    return TypeIdText(
        obj['type'],
        obj['id'],
        obj['text']
    );
  }

  int get idAsLong => int.parse(id);

  static List<TypeIdText> listFrom(List<dynamic> list) {
    return list.map((e) => TypeIdText.fromMap(e)).toList();
  }

  Map<String, dynamic> toJson() {
    return {
      'type': type,
      'id': id,
      'text': text
    };
  }

  String get typeId => '$type-$id}';
}

class Result
{
  bool success;
  String msg;

  Result(this.success, this.msg);

  Result.failureMsg(this.msg): success = false;
  Result.successMsg(this.msg): success = true;
}