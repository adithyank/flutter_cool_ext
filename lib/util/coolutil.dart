import 'dart:collection';
import 'package:cool_ext/cool_ext.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class CoolUtil {

  static InputBorder outlinedRounded25rNoneBorder = OutlineInputBorder(
    borderRadius: BorderRadius.circular(25),
    borderSide: BorderSide.none
  );

  static Widget whiteContainer() {
    return Container(color: Colors.white);
  }

  static Widget networkErrorPage({String? errorMsg, Icon? icon}) {
    icon ??= const Icon(
      Icons.signal_wifi_statusbar_connected_no_internet_4_outlined,
      color: Colors.red,
    );
    if (errorMsg.isNullOrEmpty)
      errorMsg = 'Could not show the data';

    return Center(
        child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              icon,
              Text(errorMsg!).paddingAll(10)
            ]
        )
    );
  }

  static List<Widget> joinWidgets(List<Widget> widgets, Widget interleaved)
  {
    List<Widget> ret = [];

    for (var w in widgets) {
      ret.add(w);
      ret.add(interleaved);
    }

    return ret;
  }

  static List<Widget> buildAndJoinWidgets<T>({required List<T> items,
    required Widget Function(T item) widgetBuilder, required Widget interleaved,
    bool startWithInterleaver = true, bool endWithInterleaver = true,
  Widget? title})
  {
    List<Widget> ret = [];

    if (title != null) {
      ret.add(title);
    }

    if (startWithInterleaver) {
      ret.add(interleaved);
    }

    for (var i in items) {
      ret.add(widgetBuilder(i));
      ret.add(interleaved);
    }

    if (!endWithInterleaver) {
      if (ret.isNotEmpty)
        ret.removeLast();
    }

    return ret;
  }

  static List<Widget> buildAndJoinDivider<T>({required List<T> items, required Widget Function(T item) widgetBuilder})
  {
    return buildAndJoinWidgets(items: items, widgetBuilder: widgetBuilder, interleaved: const Divider(thickness: 0, height: 1));
  }

  @Deprecated('use CoolList')
  static Widget listView<T>(BuildContext context, {required List<T> items,
    required Widget Function(T item) widgetBuilder,
    String? msgOnEmpty, String? title, bool addDivider = true,
    bool startWithDivider = true, double gap = 0, bool shrinkWrap = false, Color dividerColor = Colors.grey,
  Widget? titleWidget})
  {
    msgOnEmpty = msgOnEmpty ?? 'Empty List';

    if (items.isEmpty) {
      return Center(child: Text(msgOnEmpty));
    }

    Widget interleaver = addDivider
        ? Divider(thickness: 0, height: 1, color: dividerColor)
        : SizedBox(height: gap);

    Widget child;

    child = ListView(
      padding: EdgeInsets.zero,
        shrinkWrap: shrinkWrap,
        children: buildAndJoinWidgets(
          title: titleWidget,
            items: items,
            widgetBuilder: widgetBuilder,
            interleaved: interleaver,
            startWithInterleaver: startWithDivider
        )
    ); //.onRoundedWhiteContainer();

    if (title == null || title.isEmpty) {
      return child;
    }

    return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Text(title, style: Theme.of(context).textTheme.titleLarge).paddingHorizontal(10),
          const SizedBox(height: 10),
          child.expanded()
        ]
    );
  }

  static TextStyle captionStyle(BuildContext context, {Color? foreColor})
  {
    Color fc = foreColor ?? Colors.grey.shade600;
    return Theme.of(context).textTheme.bodyMedium!.copyWith(color: fc);
  }
  
  static TextStyle disabledTextStyle(BuildContext context)
  {
    return TextStyle(color: context.theme.disabledColor);
  }

  static Widget disabledText(BuildContext context, String text, {TextAlign? textAlign})
  {
    return Text(text, style: disabledTextStyle(context), textAlign: textAlign);
  }

  static Widget boldText(BuildContext context, String text)
  {
    return Text(text, style: TextStyle(fontWeight: FontWeight.bold), softWrap: true);
  }

  static Widget captionText(BuildContext context, String text, {String? tooltip})
  {
    Widget w = Text(text, style: captionStyle(context));
    return tooltip == null || tooltip.isEmpty ? w : w.tooltip(tooltip);
  }

  static Widget clipboardCopyButton(BuildContext context, String textToCopy, {void Function(String text)? onCopied, double? iconSize, String tooltip = 'Click to Copy'})
  {
    return IconButton(
        onPressed: () {
          Clipboard.setData(ClipboardData(text: textToCopy));
          onCopied?.call(textToCopy);
        },
        icon: Icon(
            Icons.copy,
            color: context.theme.primaryColor,
            size: iconSize,
        ),
        style: ButtonStyle(
          padding: MaterialStatePropertyAll(EdgeInsets.all(5))
        ),
        constraints: BoxConstraints(),
        tooltip: tooltip
    );
  }

  static Widget clipboardCopyableText(BuildContext context, String text, {void Function(String text)? onCopied, TextStyle? style})
  {
    style = style ?? captionStyle(context);
    return Row(
        children: [
          Text(text, style: style),
          IconButton(
            visualDensity: VisualDensity.compact,
              onPressed: () {
                Clipboard.setData(ClipboardData(text: text));
                if (onCopied != null) {
                  onCopied(text);
                }
              },
              icon: Icon(
                  Icons.copy,
                  size: style.fontSize,
                  color: Theme.of(context).primaryColor
              ),
              tooltip: 'Click to copy'
          )
        ]
    );
  }

  static Widget textOnColor(BuildContext context, {required String text, bool expanded = false, TextStyle? style, Color? backgroundColor, TextAlign? textAlign, double horizontalGap = 10, double verticalGap = 10})
  {
    Widget c = Container(
      decoration: BoxDecoration(
          color: backgroundColor ?? Colors.grey.shade200,
          borderRadius: BorderRadius.circular(6)
      ),
      padding: EdgeInsets.symmetric(horizontal: horizontalGap, vertical: verticalGap),
      child: Text(
        text,
        style: style ?? Theme.of(context).textTheme.titleMedium!.copyWith(color: Colors.black87),
        textAlign: textAlign,
      ),
    );

    if (expanded) {
      return Row(children: [c.expanded()]);
    }
    else {
      return c;
    }
  }

  static Widget widgetOnColor(BuildContext context, {required Widget widget, Color? backgroundColor, EdgeInsetsGeometry? padding})
  {
    return Container(
        decoration: BoxDecoration(
            color: backgroundColor ?? Colors.grey.shade200,
            borderRadius: BorderRadius.circular(6)
        ),
        padding: padding ?? EdgeInsets.all(10),
        child: widget
    );
  }

  static Widget onInkCard({required GestureTapCallback? onTap,
    required Widget child, double radius = 10, double? elevation, Color? cardColor, EdgeInsetsGeometry? margin})
  {
    final borderRadius = BorderRadius.circular(radius);

    return Card(
      margin: margin,
      color: cardColor,
      elevation: elevation,
      shape: RoundedRectangleBorder(borderRadius: borderRadius),
      child: InkWell(
          borderRadius: borderRadius,
          //overlayColor: MaterialStatePropertyAll(cardColor),
          onTap: onTap,
          child: child,
      ),
    );
  }

  static Future<String?> inputDialog({required BuildContext context, required String title,
    String? defaultValue, String okButtonName = 'OK', String hintText = 'Type here',
    FormFieldValidator<String>? validator
  })
  {
    TextEditingController controller = TextEditingController();
    GlobalKey<FormState> formKey = GlobalKey();

    if (defaultValue.hasValue) {
      controller.text = defaultValue!;
    }

    return showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          title: Text(title),
          content: Form(
            key: formKey,
            child: TextFormField(
              controller: controller,
              validator: validator,
              decoration: InputDecoration(
                  icon: const Icon(Icons.edit_note),
                  hintText: hintText
              ),
            ),
          ),
          actions: [
            TextButton(onPressed: () {
              if (!formKey.currentState!.validate()) {
                return;
              }

              context.navigator.pop(controller.text);
            }, child: Text(okButtonName)),
            TextButton(onPressed: () => context.navigator.pop(), child: const Text('Cancel'))
          ]
        );
      }
    );

  }

  static MaterialPageRoute<T> defaultRoute<T>(Widget nextWidget)
  {
    return MaterialPageRoute<T>(builder: (context) => nextWidget);
  }


  static PageRouteBuilder<T> animatedRoute<T>(Widget nextWidget)
  {
    return PageRouteBuilder<T>(
      pageBuilder: (context, animation, secondaryAnimation) => nextWidget,
      transitionsBuilder: (context, animation, secondaryAnimation, child) {
        const begin = Offset(1.0, 0.0);
        const end = Offset.zero;
        const curve = Curves.easeInCubic;

        var tween = Tween(begin: begin, end: end).chain(CurveTween(curve: curve));

        return SlideTransition(
          position: animation.drive(tween),
          child: child,
        );
      },
    );
  }
  
  static Widget wrapInFormField<T>({required FormFieldValidator<T>? validator,
    required Widget child, String? labelText, bool? filled, InputBorder? border, double childTopPadding = 5,
    EdgeInsetsGeometry? contentPadding,
  Widget? suffixIcon})
  {
    return FormField(
        validator: validator,
        builder: (field) {
          return InputDecorator(
              decoration: InputDecoration(
                //isDense: true,
                contentPadding: contentPadding ?? EdgeInsets.zero,
                border: border,
                //fillColor: Colors.transparent,
                errorText: field.errorText,
                //errorBorder: InputBorder.none,
                labelText: labelText,
                  //border: InputBorder.none,
                filled: filled,
                suffixIcon: suffixIcon
              ),
            child: child.paddingTop(childTopPadding),
          );
        },
    );
  }

  static bool yesNoToBool(Object val)
  {
    if(val as String  == 'Yes') {
      return true;
    }
    return false;
  }
  
  static String? boolToYesNo(bool? val)
  {
    return val == null ? null : (val ? 'Yes' : 'No');
  }

  static double? bodyLargeSize(BuildContext context)
  {
    return context.theme.textTheme.bodyLarge?.fontSize;
  }


  static double? headlineLargeSize(BuildContext context)
  {
    return context.theme.textTheme.headlineLarge?.fontSize;
  }

  static double? headlineMediumSize(BuildContext context)
  {
    return context.theme.textTheme.headlineMedium?.fontSize;
  }

  static double? displayMediumSize(BuildContext context)
  {
    return context.theme.textTheme.displayMedium?.fontSize;
  }

  static double? displayLargeSize(BuildContext context)
  {
    return context.theme.textTheme.displayLarge?.fontSize;
  }

  static double? displaySmallSize(BuildContext context)
  {
    return context.theme.textTheme.displaySmall?.fontSize;
  }

  static void wrapInYesCancelConfirmDialog(BuildContext context, String title, {
    String? contentString,
    Widget? contentWidget,
    String action1Text = 'Yes',
    required VoidCallback action1,
    Color? action1ButtonFgColor,
    String cancelActionText = 'Cancel',
    bool Function()? preActionValidation
  })
  {
    if (contentString.isNullOrEmpty && contentWidget == null) {
      throw 'Either contentString or contentWidget should be supplied';
    }

    Widget content = contentString != null
        ? Text(contentString)
        : contentWidget!;

    showDialog(context: context, builder: (ctx) {
      return AlertDialog(
        title: Text(title),
        content: content,
        actions: [
          TextButton(
              onPressed: () {
                bool execute = preActionValidation == null || preActionValidation();
                if (execute) {
                  action1();
                  Navigator.pop(ctx);
                }
              },
              style: ButtonStyle(
                  foregroundColor: MaterialStatePropertyAll(action1ButtonFgColor)
              ),
              child: Text(action1Text)
          ),
          TextButton(
              onPressed: () => Navigator.pop(ctx), child: Text(cancelActionText)),
        ],

      );
    });
  }

  static Widget centerText(String text)
  {
    return Center(
      child: Text(text),
    );
  }

}

class OneTimeWorker
{
  static final Set<String> _done = HashSet();
  
  static void work({required String jobName, required VoidCallback action})
  {
    if (_done.contains(jobName)) {
      return;
    }

    _done.add(jobName);
    action();
  }
}

class ProcessResult<T>
{
  bool success;
  String? msg;
  T? obj;

  @Deprecated('use successMsg()')
  ProcessResult.success(this.obj): success = true;

  @Deprecated('use failureMsg()')
  ProcessResult.fail(this.msg): success = false;

  ProcessResult.successMsg(this.msg): success = true;
  ProcessResult.failureMsg(this.msg): success = false;
  ProcessResult.empty(): this.successMsg('');
  ProcessResult({required this.success, required this.msg});

  @override
  String toString() {
    return 'ProcessResult: success: $success, msg: $msg, obj: $obj';
  }
}

class MsgDisplay<T> extends StatelessWidget {
  final ValueNotifier<ProcessResult<T>> valueListenable;
  final TextAlign? textAlign;

  const MsgDisplay({required this.valueListenable, this.textAlign, super.key});

  @override
  Widget build(BuildContext context) {
    return ValueListenableBuilder(
      valueListenable: valueListenable,
      builder: (context, value, child) {
        return Text(
            value.msg ?? '',
            textAlign: textAlign ?? TextAlign.center,
            style: TextStyle(color: value.success ? Colors.blue : Colors.red)
        );
      },
    );
  }
}
