import 'package:intl/intl.dart';

extension NumExt on num
{
  static NumberFormat doublePrettyFormat = NumberFormat('#.##');

  String get asTwoDecimals => doublePrettyFormat.format(this);
}