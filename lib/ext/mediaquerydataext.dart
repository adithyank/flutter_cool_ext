import 'dart:math';

import 'package:flutter/material.dart';

extension MediaQueryDataExt on MediaQueryData
{
  bool get isLandscape {
    Size s = size;
    return s.width > s.height;
  }

  bool get isPortrait => !isLandscape;

  double get shorterSize {
    Size s = size;
    return min(s.width, s.height);
  }

  double get largerSize {
    Size s = size;
    return max(s.width, s.height);
  }

  double get halfScreenWidth  => size.width / 2;
  double get halfScreenHeight => size.height / 2;
}

