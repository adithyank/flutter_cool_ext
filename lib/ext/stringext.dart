extension NullableStringExt on String?
{
  bool get hasValue => this != null && this!.isNotEmpty;

  bool get isNullOrEmpty => this == null || this!.isEmpty;

  String mapIfEmpty(String str)
  {
    return isNullOrEmpty ? str : this!;
  }
}

extension StringExt on String
{
  static RegExp regexNumbersOnly = RegExp(r'^[0-9]+$');
  static RegExp regexStartsWithPlusAndThenOnlyNumbers = RegExp(r'^\+[0-9]+$');

  String takeLeft(int characterCount, {bool addEllipses = false})
  {
    if (length <= characterCount) {
      return this;
    }

    String sub = substring(0, characterCount);
    return addEllipses ? '$sub...' : sub;
  }

  String takeAfter(String search)
  {
    int index = indexOf(search);
    if (index < 0) {
      return '';
    }
    else {
      return substring(index + search.length);
    }
  }

  String takeAfterLast(String search)
  {
    int index = lastIndexOf(search);
    return index < 0 ? '' : substring(index + search.length);
  }

  bool isAnyOf(List<String> items) {
    return items.contains(this);
  }

  bool get containsOnlyNumbers
  {
    return regexNumbersOnly.hasMatch(this);
  }
  
  bool get startsWithPlusAndContainsOnlyNumbers
  {
    return regexStartsWithPlusAndThenOnlyNumbers.hasMatch(this);
  }
}

