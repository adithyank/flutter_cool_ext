import 'package:flutter/material.dart';

extension IconExt on Icon
{
  Icon withSize(double size)
  {
    return Icon(icon, size: size, color: color);
  }

  Icon withColor(Color customColor)
  {
    return Icon(icon, color: customColor);
  }
}