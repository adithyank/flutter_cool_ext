extension DynamicExt on dynamic
{
  R? mapIfNotNull<I, R>(R Function(I input) mapper, {R? defaultValue})
  {
    return this == null
        ? defaultValue
        : mapper(this);
  }
}

