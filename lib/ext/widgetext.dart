import 'package:cool_ext/cool_ext.dart';
import 'package:flutter/material.dart';

extension WidgetExt on Widget
{

  Padding paddingCustom(EdgeInsetsGeometry padding)
  {
    return Padding(padding: padding, child: this);
  }

  Padding paddingTLBR(double top, double left, double bottom, double right)
  {
    return Padding(padding: EdgeInsets.only(top: top, left: left, bottom: bottom, right: right), child: this);
  }

  Padding paddingTop(double value)
  {
    return Padding(padding: EdgeInsets.only(top: value), child: this);
  }

  Padding paddingRight(double value)
  {
    return Padding(padding: EdgeInsets.only(right: value), child: this);
  }

  Padding paddingBottom(double value)
  {
    return Padding(padding: EdgeInsets.only(bottom: value), child: this);
  }

  Padding paddingLeft(double value)
  {
    return Padding(padding: EdgeInsets.only(left: value), child: this);
  }

  Padding paddingAll(double value)
  {
    return paddingCustom(EdgeInsets.all(value));
  }

  Padding paddingVH(double verticalPadding, double horizontalPadding)
  {
    return paddingCustom(EdgeInsets.symmetric(horizontal: horizontalPadding, vertical: verticalPadding));
  }

  Padding paddingHorizontal(double value)
  {
    return paddingCustom(EdgeInsets.symmetric(horizontal: value));
  }

  Padding paddingVertical(double value)
  {
    return paddingCustom(EdgeInsets.symmetric(vertical: value));
  }

  Expanded  expanded({int? flex})
  {
    if (flex == null) {
      return Expanded(child: this);
    } else {
      return Expanded(flex: flex, child: this);
    }
  }

  Flexible flexible({int? flex, FlexFit? flexFit})
  {
    if (flex == null && flexFit == null) {
      return Flexible(child: this);
    } else if (flex != null && flexFit == null) {
      return Flexible(flex: flex, child: this);
    } else if (flex == null && flexFit != null) {
      return Flexible(fit: flexFit, child: this);
    } else {
      return Flexible(flex: flex!, fit: flexFit!, child: this);
    }

  }
  
  Widget tooltip(String? text, {TooltipTriggerMode? tooltipTriggerMode})
  {
    return text.isNullOrEmpty
      ? this
      : Tooltip(message: text, child: this, triggerMode: tooltipTriggerMode);
  }

  Widget tooltipMsg(String? text, {TooltipTriggerMode? tooltipTriggerMode})
  {
    return text.isNullOrEmpty
      ? this
      : Tooltip(message: text, child: this, triggerMode: tooltipTriggerMode);
  }

  Widget tooltipOnTap(String? text)
  {
    return tooltip(text, tooltipTriggerMode: TooltipTriggerMode.tap);
  }

  Container onWhiteContainer()
  {
    return Container(
        color: Colors.white,
        child: this
    );
  }

  Center onCenter()
  {
    return Center(
      child: this
    );
  }

  Container onRoundedWhiteContainer({Color? bgColor})
  {
    return Container(
        decoration: BoxDecoration(
            color: bgColor ?? Colors.white,
            borderRadius: BorderRadius.all(Radius.circular(10))
        ),
        child: this
    );
  }

  Widget onScroll()
  {
    return SingleChildScrollView(child: this);
  }

  Widget border(BuildContext context, {double radius = 5, EdgeInsetsGeometry padding = const EdgeInsets.all(5), Color? borderColor})
  {
    borderColor ??= Theme.of(context).disabledColor;

    return Container(
        padding: padding,
        decoration: BoxDecoration(
            border: Border.all(
              color: borderColor,
            ),
            borderRadius: BorderRadius.circular(radius)
        ),
        child: this
    );
  }

  Widget onSafeArea()
  {
    return SafeArea(child: this);
  }


}
