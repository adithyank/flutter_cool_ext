import 'package:flutter/cupertino.dart';

extension NullableIterableExt<E> on Iterable<E>?
{
  bool get hasValue => !isNullOrEmpty;
  bool get isNullOrEmpty => this == null || this!.isEmpty;

  List<R> mapToList<R>(R Function(E item) mapperFunction) {
    return isNullOrEmpty
        ? []
        : this!.map((e) => mapperFunction(e)).toList();
  }

  List<R>? mapToNullableList<R>(R Function(E item) mapperFunction) {
    return isNullOrEmpty
        ? null
        : this!.map((e) => mapperFunction(e)).toList();
  }


  Map<GK, List<E>> group<GK>(GK Function(E item) groupKeyProvider)
  {
    Map<GK, List<E>> map = {};

    if (this == null) {
      return map;
    }

    for (var item in this!) {
      GK key = groupKeyProvider(item);
      map.putIfAbsent(key, () => []).add(item);
    }

    return map;
  }

  Map<K, E> toMap<K>(K Function(E item) keyProvider)
  {
    Map<K, E> map = {};
    if (this == null) return map;

    for (var item in this!) {
      map[keyProvider(item)] = item;
    }

    return map;
  }

}

extension WidgetListExt on List<Widget>?
{
  List<Widget> joinWith(Widget item)
  {
    if (this == null) {
      return List.empty();
    }

    List<Widget> ret = [];

    for (int i = 0; i < this!.length; i++) {
      ret.add(this![i]);
      if (i < this!.length - 1) {
        ret.add(item);
      }
    }

    return ret;
  }
}