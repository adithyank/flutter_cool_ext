
import 'package:flutter/material.dart';

import '../util/coolutil.dart';

extension NavigatorExt on NavigatorState
{
  Future<T?> pushWidget<T>(Widget widget)
  {
    return push(
        NavUtil.route(widget)
    );
  }

  Future<T?> pushReplacementWidget<T>(Widget widget)
  {
    return pushReplacement(
        NavUtil.route(widget)
    );
  }

  void popUntilFirst<T>()
  {
    popUntil((route) => route.isFirst);
  }

  void removeUntilFirstAndPushReplacementWidget(Widget widget)
  {
    pushAndRemoveUntil(NavUtil.route(widget), (route) => false);
    // popUntilFirst();
    // pushReplacementWidget(widget);
  }
}

enum PushRouteAnimation
{
  defaultAnimation,
  rightToLeft
}

class NavUtil
{
  static PushRouteAnimation pushRouteAnimation = PushRouteAnimation.defaultAnimation;

  static Route<T> route<T>(Widget next)
  {
    return pushRouteAnimation == PushRouteAnimation.defaultAnimation
        ? CoolUtil.defaultRoute(next)
        : CoolUtil.animatedRoute(next);
  }
}
